import os
import math
import pprint
from datetime import datetime, date
from flask import Flask, render_template, request, redirect, flash
import googlemaps

from .helpers import load_database, forecast


"""
Init app and configure enviromental variables
"""
app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY')
googlemaps_client_key = os.environ.get('GOOGLEMAPS_CLIENT_KEY')
googlemaps_server_key = os.environ.get('GOOGLEMAPS_SERVER_KEY')
forecast_cap = 60



"""
Build dict database from CSV file
"""
data = load_database()
trip_lenght_cap = data['settings']['trip_lenght_cap']
trip_lenght_baseline = data['settings']['trip_lenght_baseline']



"""
App routes
"""
@app.route('/')
def index():
    context = { 'googlemaps_client_key': googlemaps_client_key}
    return render_template('index.html', context=context)


@app.route('/backpack', methods=["GET", "POST"])
def backpack():
  
    # User reached route via POST
    if request.method == "POST":

        # Check if destination is geocoded
        if not request.form.get('destination'):
            flash('Missing destination')
            return redirect('/')
        else:
            destination = request.form.get('destination')
        
        if not request.form.get('destination_lat') or not request.form.get('destination_lng'):
            try:
                gmaps = googlemaps.Client(key=googlemaps_server_key)
                geocode_response = gmaps.geocode(destination)
                destination_lat = geocode_response[0]['geometry']['location']['lat']
                destination_lng = geocode_response[0]['geometry']['location']['lng']
            except:
                flash('Invalid destination')
                return redirect('/')
        else:
            destination_lat = request.form.get('destination_lat')
            destination_lng = request.form.get('destination_lng')

        # Calculate total trip duration
        try:
            date_start = datetime.strptime(request.form.get('date_start'), '%Y-%m-%d').date()
        except:
            flash('Invalid start date')
            return redirect('/')

        try:
            date_end = datetime.strptime(request.form.get('date_end'), '%Y-%m-%d').date()
        except:
            flash('Invalid end date')
            return redirect('/')
        
        if date_end <= date_start:
            flash('End date needs to be after start date')
            return redirect('/')

        trip_lenght = (date_end - date_start).days

        if trip_lenght > forecast_cap:
            flash(f'Forecast limited to {forecast_cap} days of trip lenght')
            return redirect('/')

        trip_lenght_cap = data['settings']['trip_lenght_cap']
        trip_lenght_baseline = data['settings']['trip_lenght_baseline']

        # Get average weather temperature from trip timespan
        try:
            forecast_days = forecast(destination_lat, destination_lng, date_start, date_end)
        except:
            flash('Forecast not available.')
            return redirect('/')

        try:
            temperature = 0.0
            for day in forecast_days:
                temperature += float(day['feelslike'])
            temperature = round(temperature / len(forecast_days), 1)
        except:
            flash("Could't calculate average temperature.")
            return redirect('/')

        for scale in data['temperatures']:
            if scale['start'] <= temperature <= scale['end']:
                temperature_range = scale['name']

        # Check if gender is defined
        try:
            gender = request.form['gender']
        except:
            flash('Invalid gender')
            return redirect('/')

        # Build result list based on product constrains
        result_backpack = []
        result_categories = []
        for product in data['products']:

            if product['for_boy'] and gender != 'boy':
                continue
            if product['for_girl'] and gender != 'girl':
                continue
            
            item_qtd = product[temperature_range]

            if product['variable_by_day']:
                if trip_lenght > trip_lenght_cap:
                    item_qtd = item_qtd * trip_lenght_cap / trip_lenght_baseline
                else:
                    item_qtd = item_qtd * trip_lenght / trip_lenght_baseline

            if product['always_take_one']:
                item_qtd = 1

            item_qtd = math.ceil(item_qtd)
            
            if item_qtd > 0:
                result_backpack.append({
                    'category'      : product['category'],
                    'name'          : product['name'],
                    'description'   : product['description'],
                    'image'         : product['image'],
                    'qtd'           : item_qtd,
                    })
                if product['category'] not in result_categories:
                    result_categories.append(product['category'])

        context = {
            'destination'       : destination,
            'destination_lat'   : destination_lat,
            'destination_lng'   : destination_lng,
            'temperature'       : temperature,
            'temperature_range' : temperature_range,
            'date_start'        : date_start,
            'date_end'          : date_end,
            'trip_lenght'       : trip_lenght,
            'gender'            : gender,
            'result_backpack'   : result_backpack,
            'categories'        : result_categories
        }

        if os.environ.get('FLASK_ENV') == 'development':
            pprint.pprint(context, width=256, sort_dicts=False)

        return render_template('list.html', context=context)

    # User reached route via GET
    else:
        return redirect('/')

# TODO: Add route for product detail