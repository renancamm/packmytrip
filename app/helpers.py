import os
import csv
import requests
import pprint
from datetime import datetime

"""
Build dict database from csv file
"""

# Helpers to parse csv strings to data types
def csv_string2bool(string):
    return True if string == 'TRUE' else False

def csv_string2float(string):
    return float(string) if string != '' else 0.0

 # Load CSV files
def load_database():

    # Init containers
    products = []
    temperatures = []
    settings = {}

    with open('app/data/products.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:

            # Build list of product dicts
            products.append({
                'name'              : row['name'],
                'category'          : row['category'],
                'for_boy'           : csv_string2bool(row['for_boy']),
                'for_girl'          : csv_string2bool(row['for_girl']),
                'always_take_one'   : csv_string2bool(row['always_take_one']),
                'variable_by_day'   : csv_string2bool(row['variable_by_day']),
                'qtd_very_cold'     : csv_string2float(row['qtd_very_cold']),
                'qtd_cold'          : csv_string2float(row['qtd_cold']),
                'qtd_cool'          : csv_string2float(row['qtd_cool']),
                'qtd_mild'          : csv_string2float(row['qtd_mild']),
                'qtd_warm'          : csv_string2float(row['qtd_warm']),
                'qtd_hot'           : csv_string2float(row['qtd_hot']),
                'qtd_very_hot'      : csv_string2float(row['qtd_very_hot']),
                'image'             : row['image'],
                'description'       : row['description']
            })

    with open('app/data/temperatures.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # Build list of temperature range dicts
            temperatures.append({
                'name'  : row['name'],
                'start' : csv_string2float(row['start']),
                'end'   : csv_string2float(row['end'])
            })

    with open('app/data/settings.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # Build dict of constants
            settings[row['key']] = int(row['value'])
    
    # Return all data into a single dict
    return {
        'products'      : products,
        'temperatures'  : temperatures,
        'settings'      : settings
    }



def forecast(lat, lng, date_start, date_end):
    # Contact API
    date_start = date_start.isoformat()
    date_end = date_end.isoformat()
    api_key = os.environ.get('VISUALCROSSING_KEY')
    include = 'obs,fcst,stats'
    unitGroup = 'metric'
    endpoint = f'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/{lat},{lng}/{date_start}/{date_end}?key={api_key}&include={include}&unitGroup={unitGroup}'

    response = requests.get(endpoint)
    print(f'[{datetime.now()}] - Request "{endpoint}" - {response.status_code}')
    response.raise_for_status()

    # Parse response
    forecast = response.json()
    forecast_days = forecast['days']
        
    if os.environ.get('FLASK_ENV') == 'development':
        pprint.pprint(forecast, width=256, sort_dicts=False)

    return forecast_days