function init() {
  initAutocomplete();
  initUpdateDate();
}



function initAutocomplete() {

  // '''''''''''''''''''''''''''' //
  //  Google Places autocomplete  //
  // ,,,,,,,,,,,,,,,,,,,,,,,,,,,, //

  // Configure the HTML elements to apply Google Place Widget
  const input = 'destination';
  const hiddenLat = 'destination_lat';
  const hiddenLng = 'destination_lng';
  const form = 'form';

  // Apply widget
  var elementInput = document.getElementById(input);
  var widget = new google.maps.places.Autocomplete(elementInput);

  // Add listener to apply hidden geocode
  var elementLat = document.getElementById(hiddenLat);
  var elementLng = document.getElementById(hiddenLng);
  widget.addListener('place_changed', function () {
    var place = widget.getPlace();
    if (place.geometry) {
      elementLat.value = place.geometry.location.lat();
      elementLng.value = place.geometry.location.lng();
    }
  });

  // Add listener to prevent submit on place selection
  var elementForm = document.getElementById(form);
  elementForm.addEventListener('submit', function (event) {
    if (document.activeElement == elementInput) {
      event.preventDefault();
    }
  });
}



function initUpdateDate() {

  // '''''''''''''''''''''''' //
  //  Auto update date field  //
  // ,,,,,,,,,,,,,,,,,,,,,,,, //

  // Configure the HTML elements to update date
  const start = 'date_start';
  const end = 'date_end';
  const endDaysOffset = 7;
  const maxYearsOffset = 1;

  // Get element DOM for each data field
  elementStart = document.getElementById(start);
  elementEnd = document.getElementById(end);

  // Update start with today's date.
  var dateStart = new Date();
  elementStart.value = formatDate(dateStart);

  // Add listener to auto-update the endDate based on startDate + offset
  elementStart.addEventListener('change', function () {
    var dateEnd = new Date(elementStart.value);
    dateEnd.setDate(dateEnd.getDate() + endDaysOffset);
    elementEnd.value = formatDate(dateEnd);
  });

  // Set min/max dates for both fields based on today's date
  var min = formatDate(dateStart);
  var max = dateStart;
  max.setFullYear(max.getFullYear() + maxYearsOffset);
  max = formatDate(max);
  elementStart.setAttribute('min', min);
  elementStart.setAttribute('max', max);
  elementEnd.setAttribute('min', min);
  elementEnd.setAttribute('max', max);
}



// Helper function to print ISO date without time
function formatDate(date) {
  var year = date.getFullYear().toString();
  var month = (date.getMonth() + 1).toString().padStart(2, 0);
  var day = date.getDate().toString().padStart(2, 0);
  return year + '-' + month + '-' + day;
}